<?php 
require_once 'rss/rss_php.php';
require_once 'html2text-master/html2text.php';

define("TOKEN", "244183563:AAGwYmiVzPO1IIx32XDkTVc5IJVI0SzCasw");
define("API", "https://api.telegram.org/bot" . TOKEN);
define("SERVERNAME", "localhost");
define("USERNAME", "root");
define("PASSWORD", "districtminds");
define("DBNAME", "telegrambot");
	
updateRSS('http://rss.weather.gov.hk/rss/CurrentWeather.xml', 'current', 'en');
updateRSS('http://rss.weather.gov.hk/rss/CurrentWeather_uc.xml', 'current', 'trad' );
updateRSS('http://gbrss.weather.gov.hk/rss/CurrentWeather_uc.xml', 'current', 'simp');
updateRSS('http://rss.weather.gov.hk/rss/WeatherWarningSummaryv2.xml', 'warning', 'en');
updateRSS('http://rss.weather.gov.hk/rss/WeatherWarningSummaryv2_uc.xml', 'warning', 'trad');
updateRSS('http://gbrss.weather.gov.hk/rss/WeatherWarningSummaryv2_uc.xml', 'warning', 'simp');

function updateRSS ($url, $report_type, $language) 
{
	// DB Connection
	$conn = new mysqli (SERVERNAME, USERNAME, PASSWORD, DBNAME );
	if ($conn->connect_error) {
		error_log ( 'db_connect_fail' , 0);
		exit ();
	}
	mysqli_set_charset($conn,"utf8");
	
	$rss = new rss_php;
	$rss->load($url);
	$items = $rss->getRSS();
	
	$pubDate = $items["rss"]["channel"]["item:0"]["pubDate"];
	$description = convert_html_to_text($items["rss"]["channel"]["item:0"]["description"]);
	
	$php_date = date('r', strtotime("+8 hours", strtotime($pubDate)));
	$mysql_date = date('Y-m-d H:i:s', strtotime(str_replace('-', '/', $php_date)));
	
	$sql = $conn->prepare ( "INSERT INTO RSS (report_type, description, language, pub_date) VALUES (?, ?, ?, ?)" );
	$sql->bind_param("ssss", $report_type, $description, $language, $mysql_date);
	$sql->execute ();
	if ($sql->affected_rows > 0) {
		pushReport($report_type, $language);
	}
}

function pushReport($report_type, $language)
{
	// Check User, get language if the user exists
	$conn = new mysqli (SERVERNAME, USERNAME, PASSWORD, DBNAME );
	mysqli_set_charset($conn,"utf8");
	
	if ($report_type == "current") 
	{
		$sql = $conn->prepare ( "
				SELECT u.chat_id, r.description
				FROM Users u 
				INNER JOIN RSS r ON r.language = u.language
				WHERE u.current = 1
				AND u.language = ?
				AND r.report_type = 'current'
				AND r.pub_date = 
				(
					SELECT MAX(r2.pub_date)
					FROM RSS r2
					WHERE r2.language = r.language
					AND r2.report_type = r.report_type
				)
		");
	} 
	else if ($report_type == "warning")
	{
		$sql = $conn->prepare("
				SELECT u.chat_id, r.description
				FROM Users u 
				INNER JOIN RSS r ON r.language = u.language
				WHERE u.warning = 1
				AND u.language = ?
				AND r.report_type = 'warning'
				AND r.pub_date = 
				(
					SELECT MAX(r2.pub_date)
					FROM RSS r2
					WHERE r2.language = r.language
					AND r2.report_type = r.report_type
				)
		");
	}
	
	$sql->bind_param("s", $language);
	$sql->execute ();
	$result = $sql->get_result ();
	if ($result->num_rows > 0) {
		// User exists
		while ($row = $result->fetch_array ( MYSQLI_ASSOC )) 
		{
			$chat_id = $row['chat_id'];
			$description = $row['description'];
			
			$postdata = http_build_query(
					array(
							'chat_id' => $chat_id,
							'text' => $description
					)
			);
			
			$opts = array('http' =>
					array(
							'method'  => 'POST',
							'header'  => 'Content-type: application/x-www-form-urlencoded',
							'content' => $postdata
					)
			);
			
			$context  = stream_context_create($opts);
			
			file_get_contents(API . '/sendmessage', false, $context);
		}
	}
}
?>