Welcome to JayHKOBot!

/controller/BaseController.php  : Contains the necessary methods and properties
/html2text-master               : 3rd party library to convert html to text
/rss                            : 3rd party library to mangage rss
updateRSS.php                   : Cronjob will execute this file to update RSS and push messages to subscribed users every 1 hour
webhook.php                     : Telegram Bot will execute this file when a new message comes
setwebhook.php                  : Set a new webhook file and SSL cert to Telegram