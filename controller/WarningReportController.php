<?php
require_once ("BaseController.php");

class WarningReportController extends BaseController 
{
	private $report_type = "warning";

	public function __construct($chat_id) 
	{
		parent::__construct($chat_id);
	}
	
	public function responseWarningReport() 
	{
		// Get database's RSS if exists
		$conn = new mysqli (SERVERNAME, USERNAME, PASSWORD, DBNAME );
		mysqli_set_charset($conn,"utf8");
		$sql = $conn->prepare ( "
			SELECT description
			FROM RSS
			WHERE language = ?
			AND report_type = ?
			ORDER BY pub_date DESC
			LIMIT 1");
		$sql->bind_param("ss", $this->language, $this->report_type);
		$sql->execute ();
		$result = $sql->get_result ();
		
		if ($result->num_rows > 0) 
		{
			// RSS exists
		
			$row = $result->fetch_array ( MYSQLI_ASSOC );
			$description = $row ['description'];
		} 
		else 
		{
			// RSS not exist, pull from HKO
			$rss = new rss_php;
			if ($language == "en")
			{
				$rss->load('http://rss.weather.gov.hk/rss/WeatherWarningSummaryv2.xml');
			}
			
			if ($language == "trad")
			{
				$rss->load('http://rss.weather.gov.hk/rss/WeatherWarningSummaryv2_uc.xml');
			}
			
			if ($language == "simp")
			{
				$rss->load('http://gbrss.weather.gov.hk/rss/WeatherWarningSummaryv2_uc.xml');
			}
			$items = $rss->getRSS();
		
			$description = convert_html_to_text($items["rss"]["channel"]["item:0"]["description"]);
		}
		
		$postdata = http_build_query(
				array(
						'chat_id' => $this->chat_id,
						'text' => $description
				)
		);
		
		$opts = array('http' =>
				array(
						'method'  => 'POST',
						'header'  => 'Content-type: application/x-www-form-urlencoded',
						'content' => $postdata
				)
		);
		
		$context  = stream_context_create($opts);
		
		file_get_contents(API . '/sendmessage', false, $context);
		return;
	}
}
?>