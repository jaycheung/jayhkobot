<?php 
require_once './rss/rss_php.php';
require_once './html2text-master/html2text.php';

// Telegram Bot Token
define("TOKEN", "244183563:AAGwYmiVzPO1IIx32XDkTVc5IJVI0SzCasw");
define("API", "https://api.telegram.org/bot" . TOKEN);

// Database Config
define("SERVERNAME", "localhost");
define("USERNAME", "root");
define("PASSWORD", "districtminds");
define("DBNAME", "telegrambot");

class BaseController 
{
	protected $chat_id = "";
	protected $user_exists = false;
	protected $language = "en";
	
	protected function __construct($chat_id) {
		$this->chat_id = $chat_id;
		$this->checkUserAndLanguage();
	}

	// Check User, get language if the user exists
	protected function checkUserAndLanguage() 
	{	
		$conn = new mysqli (SERVERNAME, USERNAME, PASSWORD, DBNAME );
		$conn->set_charset("utf8");
		
		$sql = $conn->prepare ( "SELECT language FROM Users WHERE chat_id = ?");
		$sql->bind_param("s", $this->chat_id);
		$sql->execute ();
		$result = $sql->get_result ();
		if ($result->num_rows > 0) 
		{
			// User exists
			$row = $result->fetch_array ( MYSQLI_ASSOC );
			$this->user_exists = true;
			$this->language  = $row ['language'];
		}
	}
	
	// Send success/fail response to User
	protected function simpleResponse ($valid_comment) 
	{
		$response = "Fail";
		
		if ($valid_comment)
		{
			if ($this->language == "en")
			{
				$response = "OK";
			}
		
			if ($this->language == "trad")
			{
				$response = "知道了";
			}
		
			if ($this->language == "simp")
			{
				$response = "知道了";
			}
		}
		else
		{
			if ($this->language == "en")
			{
				$response = "Fail";
			}
		
			if ($this->language == "trad")
			{
				$response = "錯誤";
			}
		
			if ($this->language == "simp")
			{
				$response = "错误";
			}
		}
		
		$postdata = http_build_query(
				array(
						'chat_id' => $this->chat_id,
						'text' => $response
				)
				);
		
		$opts = array('http' =>
				array(
						'method'  => 'POST',
						'header'  => 'Content-type: application/x-www-form-urlencoded',
						'content' => $postdata
				)
		);
		
		$context  = stream_context_create($opts);
		
		file_get_contents(API . '/sendmessage', false, $context);
	}
}
?>