<?php 
require_once ("BaseController.php");

class ChangeLanguageController extends BaseController
{
	public function __construct($chat_id) 
	{
		parent::__construct($chat_id);
	}
	
	public function changeLanguage($new_language) 
	{
		// DB Connection
		$conn = new mysqli (SERVERNAME, USERNAME, PASSWORD, DBNAME );
		if ($conn->connect_error) 
		{
			$this->simpleResponse(false);
		}
		
		if ($this->user_exists) 
		{
			// User exists
			$sql = $conn->prepare ( "UPDATE Users SET language = ? WHERE chat_id = ?" );
			$sql->bind_param("ss", $new_language, $this->chat_id);
		} 
		else 
		{
			// User does not exist
			error_log("user does not exist", 0);
			$this->simpleResponse(false);
		}
		
		$this->language = $new_language;
		
		if ($sql->execute())
		{
			$this->simpleResponse(true);
		}
		else
		{
			$this->simpleResponse(false);
		};
	}
}
?>