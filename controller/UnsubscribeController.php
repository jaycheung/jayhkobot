<?php 
require_once ("BaseController.php");

class UnsubscribeController extends BaseController
{
	public function __construct($chat_id) 
	{
		parent::__construct($chat_id);
	}
	public function unsubscribe($current, $warning)
	{
		// DB Connection
		$conn = new mysqli (SERVERNAME, USERNAME, PASSWORD, DBNAME );
		if ($conn->connect_error) 
		{
			error_log ( 'db_connect_fail' , 0);
			exit ();
		}
		
		if ($this->user_exists) 
		{
			// User exists
			if ($current)
			{
				$sql = $conn->prepare ( "UPDATE Users SET current = 0 WHERE chat_id = ?" );
				$sql->bind_param("s", $this->chat_id);
			}
		
			if ($warning)
			{
				$sql = $conn->prepare ( "UPDATE Users SET warning = 0 WHERE chat_id = ?" );
				$sql->bind_param("s", $this->chat_id);
			}
		} 
		else 
		{
			// User does not exist
		
			if ($current)
			{
				$sql = $conn->prepare ( "INSERT INTO Users (chat_id, current, language) VALUES (?, ?, ?)" );
				$sql->bind_param("sis", $this->chat_id, $current, $language);
			}
		
			if ($warning)
			{
				$sql = $conn->prepare ( "INSERT INTO Users (chat_id, warning, language) VALUES (?, ?, ?)" );
				$sql->bind_param("sis", $this->chat_id, $warning, $language);
			}
		}
		
		if ($sql->execute ())
		{
			$this->simpleResponse(true);
		}
		else
		{
			$this->simpleResponse(false);
		};
	}	
}
?>