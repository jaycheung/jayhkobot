<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once 'controller/CurrentReportController.php';
require_once 'controller/WarningReportController.php';
require_once 'controller/ChangeLanguageController.php';
require_once 'controller/SubscribeController.php';
require_once 'controller/UnsubscribeController.php';

// Get Updates
$content = file_get_contents("php://input");
error_log($content, 0);

// Get chat id and text
$updateArray = json_decode($content, TRUE);
$text = trim($updateArray["message"]["text"]);
$chat_id = trim($updateArray["message"]["chat"]["id"]);

// ADD NEW COMMAND HERE

// default start command
if (trim(strtolower($text)) == "/start")
{
	file_get_contents(API . '/sendmessage?chat_id=' . $chat_id . '&text=' . 'Thanks for using jayHKObot! You can type "tell me current" to check current weather report.');
	return;
}

// Ask for supporting topics
if (trim(strtolower($text)) == "topic")
{
	file_get_contents(API . '/sendmessage?chat_id=' . $chat_id . '&text=' . 'current, warning');
	return;
}

// Request Current Weather Report
if (trim(strtolower($text)) == "tellme current")
{
	$currentReportController = new CurrentReportController($chat_id);
	$currentReportController->responseCurrentReport();
}

// Request Warning Report
if (trim(strtolower($text)) == "tellme warning")
{
	$warningReportController = new WarningReportController($chat_id);
	$warningReportController->responseWarningReport();
}

// Request Language Change
if (trim(strtolower($text)) == "繁體中文" || trim(strtolower($text)) == "trad")
{
	$changeLanguageController = new ChangeLanguageController($chat_id);
	$changeLanguageController->changeLanguage("trad");
}

if (trim(strtolower($text)) == "简体中文" || trim(strtolower($text)) == "simp")
{
	$changeLanguageController = new ChangeLanguageController($chat_id);
	$changeLanguageController->changeLanguage("simp");
}

if (trim(strtolower($text)) == "English" || trim(strtolower($text)) == "en")
{
	$changeLanguageController = new ChangeLanguageController($chat_id);
	$changeLanguageController->changeLanguage("en");
}

// Subscribe Warning
if (trim(strtolower($text)) == "subscribe warning") 
{
	$subscribeController = new SubscribeController($chat_id);
	$subscribeController->subscribe(0, 1);
}

// Subscribe Current
if (trim(strtolower($text)) == "subscribe current")
{
	$subscribeController = new SubscribeController($chat_id);
	$subscribeController->subscribe(1, 0);
}

// Unsubscribe Warning
if (trim(strtolower($text)) == "unsubscribe warning")
{
	$unsubscribeController = new UnsubscribeController($chat_id);
	$unsubscribeController->unsubscribe(0, 1);
}

// Unsubscribe Current
if (trim(strtolower($text)) == "unsubscribe current")
{
	$unsubscribeController = new UnsubscribeController($chat_id);
	$unsubscribeController->unsubscribe(1, 0);
}

?>